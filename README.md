PlexFreeMem is a small Python program which monitors your Plex Media Server
transcode cache and removes older transcode segments on a specified interval.

This program is designed to be used in conjunction with Plex Media Server 
configured to perform transcodes on a RAM disk. It enables the user to designate 
a relatively small amount of memory for the RAM disk (and benefit from low
latency, no disk thrashing, etc.) without the disk filling during the 
transcoding process.

PlexFreeMem behaves like a rolling queue, except it prunes old transcode segments
on a specified interval rather than immediately, so that the performance impact
is negligible. This interval can be adjusted by the user but defaults to 60
seconds. When the time elapses, PlexFreeMem checks whether the fill threshold
(70% by default) is exceeded. If the threshold is exceeded, PlexFreeMem removes
the oldest one half of the active transcode segments. If multiple simultaneous 
transcodes are occurring, PlexFreeMem will prune half of the transcoded segments
from each transcode subdirectory.

PlexFreeMem can be launched from the Windows command prompt by executing the
plex_free_mem module, with options for selecting the logging level, disk check
interval, and disk fill threshold.

PlexFreeMem does not support UNIX in this first version.
