""" PlexFreeMem v1.0_11-1-2015 by William Bernbaum

This module defines the Config class which wraps several mutable program settings.

"""
import os
from winreg import *

class Config(object):
	"""	Encapsulates multiple program settings
	
	When instantiated, Config obtaines the Plex transcode path from the Windows registry.
	Raises an exception when instantiated on a non-Windows platform.

	Attributes:
		interval (int): Period in seconds between disk threshold checks. The default
			interval is 60 seconds. Higher quality source media and a smaller disk size 
			require more frequent threshold checks. A higher disk threshold (speficied 
			by the user) will also require more frequent checks. The user may adjust this 
			attribute manually, or via command prompt with option '--interval'.
			CAUTION: Forcing a low interval may negatively impact performance. The interval
			attribute should not be set to less than 30 seconds.
		threshold (int): The limit which must be exceeded before garbage collection begins.
			This threshold is measured in units out of 1000. The default is 700, which
			corresponds to a disk that is 70.0% full. The user may adjust this attribute
			manually, or via command prompt with option '--threshold'. Reasonable values
			are between 500 and 900. A higher threshold value requires a lower interval
			(more frequent disk	checks) to prevent the disk from filling.
		transcode_ext	(str): The file extension used for transcoded segments. This is
			'.ts' by default, and should not be modified by the user.
		drive_path (str): Drive letter path specifies the disk to query. This path 
			is obtained	from the appropriate Plex registry key. To modify this attribute,
			see below.
		transcode_path (str): Path to the temporary working directory used by the 
			Plex New Transcoder. This path is obtained from the appropriate Plex 
			registry key. The transcode_path can be modified from the Plex web 
			interface, under Settings -> Server -> General -> Advanced.

	"""

	interval = 60			# Period in seconds between garbage collection checks
	threshold = 700			# When disk fills past _ / 1000, begin garbage collection
	transcode_ext = '.ts'	# Extension used by Plex New Transcoder
	drive_path = ''			# Disk to monitor
	transcode_path = ''		# Directory used for plex transcode cache
	
	def __init__(self):
		if os.name == 'nt':
			plex_reg = ConnectRegistry(None, HKEY_CURRENT_USER)
			plex_key = OpenKey(plex_reg, r"SOFTWARE\Plex, Inc.\Plex Media Server")
			for index in range(0, QueryInfoKey(plex_key)[1]):
				try:
					value_name = EnumValue(plex_key, index)[0]
					if (value_name == "TranscoderTempDirectory"):
						self.transcode_path = QueryValueEx(plex_key, value_name)[0]
						self.drive_path = self.transcode_path[:2]
						break
				except OSError:
					break
			CloseKey(plex_key)
		else:
			raise Exception("platform not supported")
	
	# Returns an int containing the disk check interval in seconds
	def get_interval(self):
		return self.interval
	
	# Returns an int containing the disk fill threshold in units out of 1000
	def get_threshold(self):
		return self.threshold
	
	# Returns a string with the drive letter path for the disk containing the
	# Plex transcode directory
	def get_disk(self):
		return self.drive_path
	
	# Returns a string containing the path for the Plex transcode directory
	def get_path(self):
		return self.transcode_path
	
	# Returns a string containing the file extension used by the Plex New Transcoder
	def check_file_ext(self, filename):
		return self.transcode_ext == os.path.splitext(filename)[1]

	def set_interval(self, interval):
		""" Updates the interval atrribute with the provided value

		Args:
			param1 (int): Accepts an int X such that 1 <= X < 600

		Raises a ValueError if the input is out of range
		"""
		if 1 <= interval < 600:
			self.interval = interval
		else:
			raise ValueError("Input out of range")

	def set_threshold(self, threshold):
		""" Updates the threshold atrribute with the provided value

		Args:
			param1 (int): Accepts an int X such that 100 <= X < 900

		Raises a ValueError if the input is out of range
		"""
		if 100 <= threshold < 900:
			self.threshold = threshold
		else:
			raise ValueError("Input out of range")
			