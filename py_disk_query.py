""" PlexFreeMem v1.0_11-1-2015 by William Bernbaum

This module queries a disk within the Windows environment. Functions of PyDiskQuery
utilitze the ctypes library to return the total and used bytes of the given disk.

"""
import os, ctypes
from ctypes import windll, c_uint64, byref, WinError

class PyDiskQuery(object):
	"""	Checks capacity and usage of the specified disk.
	
	PyDiskQuery accepts a logging.Logger object and a config.Config() object
	when instantiated.
	
	Raises an exception when instantiated on a non-Windows platform.
	
	Args:
		param1 (object): logging.Logger is a simple implementaion of the 
			logging	facility for Python
		param2 (object): config.Config() wraps several mutable settings
			including drive_path which specifies the disk to query

	Attributes:
		total_bytes (int): Total capacity of the given disk in bytes
		free_bytes (int): Available capacity of the given disk in bytes
		path (str): Drive letter path specifies disk to query
		c_api (str): Python ctypes function wraps the win32 C library to 
			access disk metrics
		log (object): Logger object which implements the Python logging lib
		
	"""
	
	total_bytes = 0
	free_bytes = 0
	path = ''
	c_api = ''
	log = None

	def __init__(self, log, conf):
		if os.name == 'nt':
			self.path = conf.get_disk()
			self.c_api = windll.kernel32.GetDiskFreeSpaceExW
			self.log = log
		else:
			raise Exception("platform not supported")
	
	# Queries provided disk. Modifies two class attributes: total_bytes and free_bytes
	# Raises ctypes.WinError if the win32 function call throws an exception
	def query_disk(self):
		self.log.debug('in query_disk')
		user, total, free = ctypes.c_uint64(), ctypes.c_uint64(), ctypes.c_uint64()
		if self.c_api(self.path, ctypes.byref(user), ctypes.byref(total), ctypes.byref(free)):
			self.total_bytes, self.free_bytes = total.value, free.value
		else:
			raise ctypes.WinError()
			
	# Returns an int containing the number of free bytes
	def get_used(self):
		self.query_disk()
		return self.total_bytes - self.free_bytes

	# Returns an int containing the total number of bytes
	def get_size(self):
		self.query_disk()
		return self.total_bytes
