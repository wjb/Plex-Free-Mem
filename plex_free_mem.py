""" PlexFreeMem v1.0_11-1-2015 by William Bernbaum

PlexFreeMem is a small Python program which monitors your Plex Media Server
transcode cache and removes older transcode segments on a specified interval.

This module contains the logger, console interface, and main loop. It processes
arguments passed from the command line, logs events to file, and removes old 
transcode segments when the fill threshold is exceeded.

"""
import config, py_disk_query
import getopt, logging, os, sys, time
from operator import itemgetter

log, conf, disk = None, None, None
# By default, writes plex_free_mem.log in the current working directory
log_path = 'plex_free_mem.log'

def init_logger():
	""" Creates the Logger. Sets up formatting and default logging level
	
	Python's logging module supports multiple levels of severity. By default,
	the level is set to INFO. The supported values are CRITICAL, ERROR,	WARNING, 
	INFO, and DEBUG, in order of decreasing severity. The logger will only log
	events labeled with the specified severity or higher. The DEBUG setting
	causes all events to be logged.
	
	Returns:
		A Logger which logs events to the file specified by log_path
	
	"""
	fh = logging.FileHandler(log_path)
	formatter = logging.Formatter(fmt='%(levelname)s:%(name)s: %(asctime)s.%(msecs).04d : %(message)s', 
		datefmt='%Y-%m-%d %H:%M:%S')
	fh.setFormatter(formatter)

	logger = logging.getLogger()
	logger.setLevel(logging.INFO)
	logger.addHandler(fh)
	return logger

# Initializes the Logger. Instantiates config.Config and py_disk_query.PyDiskQuery
# Raises an exception if the modules could not be loaded.
try:
	log = init_logger()
	conf = config.Config()
	log.info('Config module loaded')
	disk = py_disk_query.PyDiskQuery(log, conf)
	log.info('Disk query module loaded')
except Exception as e:
	sys.exit('Unknown error occurred: Could not initialize modules')

# Usage function which prints a program description to console and exits
def usage():
	print (
		"Usage: \"" + sys.argv[0] + " [-Options...]\n"
		"Options:\n"
		"--help, -h\t\t"
		"Display usage and full list of options")
	log.info('Printed usage info to console. Program exiting')
	sys.exit()

# Verbose usage function which prints a program description to console and exits
def usage_verbose():
	print (
		"Usage: \"" + sys.argv[0] + " [-Options...]\n"
		"Options:\n"
		"--help, -h\n\t\t\t"
		"Display this information and exit\n"
		"--log, -l\n\t\t\t"
		"Logging level. Default behavior: INFO\n\t\t\tAccepts DEBUG, INFO, WARNING, ERROR\n"
		"--interval, -i\n\t\t\t"
		"Interval in seconds between disk checks. Default: 60\n\t\t\tAccepts a number between 1 and 600\n"
		"--threshold, -t\n\t\t\t"
		"Disk fill level out of 1000. Begins garbage collect\n\t\t\twhen this number is exceeded. Default: 700\n\t\t\tAccepts a number between 100 and 900")
	log.info('Printed usage info to console. Program exiting')
	sys.exit()

try:
	opts, args = getopt.getopt(sys.argv[1:],
		'hl:i:t:', ['help', 'log=', 'interval=', 'threshold='])
except getopt.GetoptError as e:
	log.error('Malformed console argument. PlexFreeMem not initiated')
	print(e)
	usage()
else:
	if len(args) != 0:
		log.error('Malformed console argument. PlexFreeMem not initiated')
		print("Incorrect argument provided")
		usage()
	if len(opts) == 0:
		print("PlexFreeMem configured with default options")
	
# Parses options from console and sets program attributes if values are complaint 
for o, a in opts:
	if o in ("-h", "--help"):
		usage_verbose()
	elif o in ("-l", "--log"):
		try:
			log.setLevel(getattr(logging, a.upper()))
		except Exception as e:
			log.warning('Malformed input. Using default log level: ' + str(e))
		else:
			log.info('Logging level set to ' + a)
	elif o in ("-i", "--interval"):
		try:
			conf.set_interval(int(a))
		except Exception as e:
			log.warning('Malformed input. Using default interval: ' + str(e))
		else:
			log.info('Disk check interval set to ' + str(a) + ' seconds')
	elif o in ("-t", "--threshold"):
		try:
			conf.set_threshold(int(a))
		except Exception as e:
			log.warning('Malformed input. Using default threshold: ' + str(e))
		else:
			log.info('Threshold set to ' + str(a))
	else:
		log.critical('Unhandled option. PlexFreeMem not initiated')
		raise Exception("Unhandled Option")
	
def check_thresh():
	""" Queries the disk and returns True if the fill threshold is exceeded
	
	Returns:
		True if the ratio of used bytes to total bytes is greater than 
		Config.get_threshold() divided by 1000. By default, Config.get_threshold()
		returns 700.
		If threshold is not exceeded, returns False

	"""
	log.debug('in check_thresh')
	thresh = disk.get_used() / disk.get_size()
	return (thresh > conf.get_threshold() / 1000)

# Main program loop with garbage collecting logic
def main():
	log.debug('in main')
	thresh_exceeded = False
	while True:
		thresh_exceeded = check_thresh()
		if thresh_exceeded:
			log.info('Disk has reached threshold. Initiating garbage collect')
			active_transcodes = next(os.walk(conf.get_path()))[1]
			if active_transcodes:
				for dir in active_transcodes:
					current_path = os.path.join(conf.get_path(), dir)
					log.debug('Analyzing directory <' + current_path +'>')
					files = [f for f in os.listdir(current_path) if 
						os.path.isfile(os.path.join(current_path, f))]
					files = [f for f in files if conf.check_file_ext(f)]
					timestamps = []
					for name in files:
						file = os.path.join(current_path, name)
						try:
							m_time = os.path.getmtime(file)
							timestamps.append((file, m_time))
						except Exception as e:
							log.warning('Could not access file: ' + str(e))
					timestamps = sorted(timestamps, key=itemgetter(1))
					log.debug('Removing least current transcodes')
					for i in range(len(timestamps) // 2):
						try:
							os.remove(timestamps[i][0])
						except Exception as e:
							log.warning('Could not remove file: ' + str(e))
					log.debug('Garbage collection complete')
			else:
				log.warning('Disk has reached threshold but plex transcode directory has no subdirectories.')
		else:
			log.debug('waiting')
			time.sleep(conf.get_interval())

if __name__ == '__main__':
	try:
		log.info('PlexFreeMem initiated')
		print("Monitoring Disk...")
		main()
	finally:
		log.info('PlexFreeMem terminated')
